﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using QuickFix;
using System.Threading;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using System.Data;
using System.Configuration;
using System.Globalization;

namespace LmeFeed
{
    class LmeCppWrapper
    {
        [DllImport(@"C:\LmeFeed\LmeFeed\Debug\LmeCppWrapper.dll", EntryPoint = "lmePassword", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
        public static extern int lmePassword([In] long millis, [In] String tFaxKeyString, [In] String tPassword, [Out] StringBuilder ePassword);
    }
    class Program
    {
        public static Boolean _dbg = false;
        static StringBuilder lmeKey = new StringBuilder(System.Configuration.ConfigurationManager.AppSettings["lmekey"]); //new StringBuilder("0108D6025DF13E6535BC57554B2299D25268F7B6492520B17EC29E762D78BCFF");
        static StringBuilder lmePwd = new StringBuilder(System.Configuration.ConfigurationManager.AppSettings["lmepwd"]); //new StringBuilder("ADMR7DCFIX");
        public static String userID = System.Configuration.ConfigurationManager.AppSettings["lmeusr"]; //"ADMR7DCFIX";
        
        public static StringBuilder sb = new StringBuilder(1000);  // Make sure this is at least 1000 chars long.
        public static String pwdID = null;
        public static long millis = 0;
        public static Session mySession = null;
        public static Boolean feedComplete = false;
        public static SocketInitiator initiator = null;

        public static void recalcPassword()
        {
            //
            // Get the system time in GMT
            TimeSpan ts = (TimeSpan)(DateTime.UtcNow - new DateTime(1970, 1, 1));
            millis = (long)ts.TotalMilliseconds;
            if (_dbg) Console.WriteLine("Millis=" + millis);

            //
            // Build an encrypted password based on the LME Crypto engine, Key and current time
            if (_dbg) Console.WriteLine("Passing in " + lmeKey.ToString() + "," + lmePwd.ToString() + "," + millis);
            LmeCppWrapper.lmePassword(millis, lmeKey.ToString(), lmePwd.ToString(), sb);
            if (_dbg) Console.WriteLine("Password (" + lmePwd + ") encrypted to " + sb.ToString());
            pwdID = sb.ToString();
        }

        static void Main(string[] args)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["dbg"] == "1")
            {
                _dbg = true;
            }

            if (_dbg) Console.WriteLine("LmeFeed");
            Boolean customDate = false;

            //
            // Parse Command Line
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] != null)
                {
                    if (args[i].Equals("-cleartradesfirst"))
                    {
                        ClientInitiator.clearTradesFirst = true;
                        Console.WriteLine("ClearingTrades first=true");
                    }
                    else if (args[i].IndexOf("date=") == 0)
                    {
                        // date in YYYYMMDD format please
                        String[] dta = args[i].Split('=');
                        ClientInitiator.datestamp = dta[1];
                        Console.WriteLine("Running for " + ClientInitiator.datestamp);
                        customDate = true;
                    }
                    else if (args[i].Equals("help"))
                    {
                        Console.WriteLine("Usage: " + args[0] + " [-clearTradesFirst] [date=YYYYMMDD]");
                        return;
                    }
                }
            }

            if (!customDate)
            {
                DateTime now = DateTime.Now;
                ClientInitiator.datestamp = now.ToString("yyyyMMdd");
            }
            if (_dbg) Console.WriteLine("Running for " + ClientInitiator.datestamp);

            //
            // Initialise the QuickFix FIX Engine
            ClientInitiator app = new ClientInitiator();
            
            String initiator_cfg = System.Configuration.ConfigurationManager.AppSettings["initiator.cfg"];
            SessionSettings settings = new SessionSettings(@initiator_cfg);
            QuickFix.Application application = new ClientInitiator();
            FileStoreFactory storeFactory = new FileStoreFactory(settings);
            ScreenLogFactory logFactory = new ScreenLogFactory(settings);
            MessageFactory messageFactory = new DefaultMessageFactory();

            initiator = new SocketInitiator(application, storeFactory, settings, logFactory, messageFactory);
            initiator.start();
            Thread.Sleep(3000);
            System.Collections.ArrayList list = initiator.getSessions();
            SessionID sessionID = (SessionID)list[0];

            //
            // Let initiation complete
            Thread.Sleep(5000);

            //
            // Request all TradeCaptureReports for the given day
            ClientInitiator.sendTradeCaptureReportRequest(sessionID);

            // Needed to pass some certiFIX tests
            //ClientInitiator.sendTestMessage(sessionID);

            //
            // Wait for feed to complete
            int mxWaits = 60 * 60 * 3; // 3 Hours
            int nWaits = 0;
            while (!feedComplete && nWaits < mxWaits)
            {
                Thread.Sleep(1000);
                nWaits++;
            }

            // Left in to help with some CertiFIX tests
            //Console.ReadLine();
            
            //
            // All Done
            initiator.stop();
        }
    }
    public class ClientInitiator : QuickFix.Application
        {
            public SessionID mySessionID = null;
            int nLogins = 0;
            public static String datestamp = "20130131";
            public static Boolean clearTradesFirst = false;
            //
            // Am keeping track of the latest esquence numbers as on a failover LME require tag 789 to be set to this value
            // Newer versions of QuickFix in Java have a configuration parameter set for this, but not in the C# world ....
            public int LatestSeqNum = 0;
            public void updateLatestSeqNum(Message msg)
            {
                try
                {
                    if (msg.isSetField(369)) LatestSeqNum = Convert.ToInt32(msg.getField(369));
                }
                catch (Exception e)
                {
                }
            }
            public static void sendTestMessage(SessionID sessionID)
            {
                //
                // Send a test message - needed for certiFIX
                try
                {
                    QuickFix44.TestRequest tr = new QuickFix44.TestRequest();
                    tr.setField(112, "1");
                    Session.sendToTarget(tr, sessionID);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            public static void sendTradeCaptureReportRequest(SessionID sessionID)
            {
                //
                // Clear existing trades
                if (clearTradesFirst) clearTrades();

                
                //
                // Request Trades again
                if (Program._dbg) Console.WriteLine("******************************************************************");
                QuickFix44.TradeCaptureReportRequest req = new QuickFix44.TradeCaptureReportRequest();
                long l = DateTime.Now.TimeOfDay.Ticks;
                req.setField(new TradeRequestID("1234"));
                req.setField(569, "1"); // All trades
                req.setField(580, "1");

                //
                // For CERTIFIX VALIDATION - add the lines in below
                //req.setField(55, "SN");
                //req.setField(569, "1");
                // END OF CERTIFIX VALIDATION
                //

                QuickFix44.TradeCaptureReportRequest.NoDates nd = new QuickFix44.TradeCaptureReportRequest.NoDates();
                nd.set(new TradeDate(datestamp));
                req.addGroup(nd);


                req.setField(453, "" + 1);
                QuickFix44.TradeCaptureReportRequest.NoPartyIDs pt = new QuickFix44.TradeCaptureReportRequest.NoPartyIDs();
                pt.setField(448, "ADM");    // Request for our Company
                pt.setField(452, "" + 4);   // Request for orders entered by us

                req.addGroup(pt);

                if (Program._dbg) Console.WriteLine("REQ=" + req.ToString());
                Session.sendToTarget(req, sessionID);
            }

            public void onCreate(SessionID value)
            {
                if (Program._dbg) Console.WriteLine("Message OnCreate --> " + value.toString());
                initialiseDataTables();
            }

            public void onLogon(QuickFix.SessionID value)
            {
                if (Program._dbg) Console.WriteLine("Called onLogon --> " + value.toString());
            }

            public void onLogout(QuickFix.SessionID value)
            {
                if (Program._dbg) Console.WriteLine("Called onLogout --> " + value.toString());
                Program.feedComplete = true;
            }

            public void toAdmin(QuickFix.Message value, QuickFix.SessionID session)
            {
                updateLatestSeqNum(value);
                if (Program._dbg)
                {
                    Console.WriteLine("----------------------------------------------------------------");
                    Console.WriteLine("Called toAdmin -->toAdmin " + value.ToString());
                    Console.WriteLine("----------------------------------------------------------------");
                }
                if (Program._dbg)
                {
                    try
                    {
                        if (value is QuickFix44.Heartbeat)
                        {
                            // Ignore
                        }
                        else
                        {
                            Console.WriteLine("Called toAdmin --> " + value.ToString());
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }

                if (isMessageOfType(value, MsgType.LOGON))
                {
                    if (Program._dbg) Console.WriteLine("*** LOGON MESSAGE ***");
                    addLogonField(value);
                }
               
            }
            private void addLogonField(QuickFix.Message message)
            {
                Program.recalcPassword();
                message.getHeader().setField(789, "" + (LatestSeqNum + 1));
                message.getHeader().setField(553, Program.userID);
                message.getHeader().setField(554, Program.pwdID);
                String milliStr = "" + Program.millis;
                message.getHeader().setField(95, "" + milliStr.Length);
                message.getHeader().setField(96, "m:" + milliStr);

                //
                // From the FIX spec it would appear that setting Tag 141=Y should reset the
                // sequence numbers, but this generally seems to mess lots of things up.  Ive
                // left this in here in case anyone ever needs to try this again.
                //if (nLogins == 0) message.getHeader().setField(141, "Y"); // Reset all Sequence Numbers
                nLogins++;
                if (Program._dbg)
                {

                    int[] tags = { 553, 554, 49, 56, 95, 96 };
                    for (int i = 0; i < tags.Length; i++)
                    {
                        try
                        {
                            Console.WriteLine(" ** TAG[" + tags[i] + "] --> " + message.getHeader().getField(tags[i]));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
            }

            private Boolean isMessageOfType(QuickFix.Message message, String type)
            {
                try
                {
                    return type.Equals(message.getHeader().getField(new MsgType()).getValue());
                }
                catch (FieldNotFound e)
                {
                    return false;
                }
            }

            public void toApp(QuickFix.Message value, QuickFix.SessionID session)
            {
                if (Program._dbg) Console.WriteLine("Called toApp --> " + value.ToString());
            }

            public void fromAdmin(QuickFix.Message value, SessionID session)
            {
                updateLatestSeqNum(value);
                if (Program._dbg)
                {
                    Console.WriteLine("----------------------------------------------------------------");
                    Console.WriteLine("Called fromAdmin -->fromAdmin " + value.ToString());
                    Console.WriteLine("----------------------------------------------------------------");
                }
            }

            public void fromApp(QuickFix.Message value, SessionID session)
            {
                updateLatestSeqNum(value);
                if (Program._dbg)
                {
                    //Console.WriteLine("----------------------------------------------------------------");
                    //Console.WriteLine("Called fromApp -->fromApp " + value.ToString());
                    //Console.WriteLine("----------------------------------------------------------------");
                }
                if (value is QuickFix42.ExecutionReport)
                {
                    //
                    // ExecutionReports will arrive as orders are entered and are picked up by the
                    // LmeOrderFeed application.  Unfortunately there is no mechanism for requesting historic
                    // orders
                    //
                }
                else if (value is QuickFix44.TradeCaptureReport)
                {
                    if (Program._dbg) Console.WriteLine("fromApp --> TradeCaptureReport");

                    //Console.ReadLine();
                    QuickFix44.TradeCaptureReport tcr = (QuickFix44.TradeCaptureReport)value;
                    if (true)
                    {
                        try
                        {
                            String CFICode = "";
                            String Symbol = "";

                            String Symbol1 = "";
                            String CFICode1 = "";
                            String PromptType1 = "";
                            String MaturityDate1="";
                            String MaturityRollingPrompt1="";
                            String Price1 = "";
                            String Qty1 = "";
                            String Side1 = "";

                            String Symbol2 = "";
                            String CFICode2 = "";
                            String PromptType2 = "";
                            String MaturityDate2 = "";
                            String MaturityRollingPrompt2 = "";
                            String Price2 = "";
                            String Qty2 = "";
                            String Side2 = "";

                            String Qty = "";
                            String Price = "";
                            String Time = "";
                            String Side = "";
                            String OrderId = "";
                            String ClOrderId = "";
                            String Party_ExecutingFirm = "";
                            String Party_ClearingFirm = "";
                            String Party_EnteringFirm = "";
                            String Party_Trader = "";
                            String InfoText = "";

                            String InternalCrossTrade = "N";
                            String SelectTradeId = "";

                            if (value.isSetField(461)) CFICode = value.getField(461);
                            if (value.isSetField(55)) Symbol = value.getField(55);
                            if (value.isSetField(58)) InfoText = value.getField(58);
                            if (value.isSetField(10020))
                            {
                                if (value.getField(10020).Equals("15")) InternalCrossTrade = "Y";
                            }
                            //if (value.isSetField(10000)) Maturity = value.getField(10000);
                            //if (value.isSetField(541)) MaturityDate = value.getField(541);
                            if (value.isSetField(32)) Qty = value.getField(32);
                            if (value.isSetField(31)) Price = value.getField(31);
                            if (value.isSetField(60)) Time = value.getField(60);
                            if (value.isSetField(10022)) SelectTradeId = value.getField(10022);
                            QuickFix44.TradeCaptureReport.NoSides group = new QuickFix44.TradeCaptureReport.NoSides();
                            QuickFix44.TradeCaptureReport.NoSides group2 = (QuickFix44.TradeCaptureReport.NoSides)tcr.getGroup(1, group);

                            if (group2.isSetField(54))
                            {
                                Side = group2.getField(54);
                                if (Side == "1") Side = "B";
                                if (Side == "2") Side = "S";
                            }
                            if (group2.isSetField(37)) OrderId = group2.getField(37);
                            if (group2.isSetField(11)) ClOrderId = group2.getField(11);
                            QuickFix44.TradeCaptureReport.NoSides.NoPartyIDs group3 = new QuickFix44.TradeCaptureReport.NoSides.NoPartyIDs();

                            //
                            // NoOfInstrumentLegs
                            int nLegs = 1;

                            if (tcr.isSetField(555))
                            {
                                try
                                {
                                    if (value.isSetField(555)) nLegs = Convert.ToInt32(tcr.getField(555));
                                }
                                catch (Exception e)
                                {
                                }
                                if (Program._dbg) Console.WriteLine("NumLegs=" + nLegs);
                                QuickFix.Group g = new QuickFix.Group(555, 1);
                                tcr.getGroup(1, g);

                                if (g.isSetField(600)) Symbol1 = g.getField(600);
                                if (g.isSetField(608)) CFICode1 = g.getField(608);
                                if (g.isSetField(10005)) PromptType1 = g.getField(10005);
                                if (g.isSetField(10002)) MaturityRollingPrompt1 = g.getField(10002);
                                if (g.isSetField(611)) MaturityDate1 = g.getField(611);
                                if (g.isSetField(624)) Side1 = g.getField(624);
                                if (g.isSetField(10003)) Qty1 = g.getField(10003);
                                if (g.isSetField(637)) Price1 = g.getField(637);

                                if (nLegs == 2)
                                {
                                    g = tcr.getGroup(2, g);
                                    if (g.isSetField(600)) Symbol2 = g.getField(600);
                                    if (g.isSetField(608)) CFICode2 = g.getField(608);
                                    if (g.isSetField(10005)) PromptType2 = g.getField(10005);
                                    if (g.isSetField(10002)) MaturityRollingPrompt2 = g.getField(10002);
                                    if (g.isSetField(611)) MaturityDate2 = g.getField(611);
                                    if (g.isSetField(624)) Side2 = g.getField(624);
                                    if (g.isSetField(10003)) Qty2 = g.getField(10003);
                                    if (g.isSetField(637)) Price2 = g.getField(637);
                                }
                            }
                            // Parties
                            for (uint p = 1; p < 5; p++)
                            {
                                try
                                {
                                    QuickFix44.TradeCaptureReport.NoSides.NoPartyIDs group4 = (QuickFix44.TradeCaptureReport.NoSides.NoPartyIDs)group2.getGroup(p, group3);
                                    String role = group4.getField(452);
                                    String val = group4.getField(448);
                                    if (role == "1") Party_ExecutingFirm = val;
                                    if (role == "4") Party_ClearingFirm = val;
                                    if (role == "7") Party_EnteringFirm = val;
                                    if (role == "36") Party_Trader = val;
                                }
                                catch (Exception e)
                                {
                                }
                            }

                            if (Program._dbg) Console.WriteLine("CFICode=" + CFICode);
                            if (Program._dbg) Console.WriteLine("Symbol=" + Symbol);
                            //if (Program._dbg) Console.WriteLine("Maturity=" + Maturity);
                            if (Program._dbg) Console.WriteLine("MaturityDate1=" + MaturityDate1);
                            if (Program._dbg) Console.WriteLine("Qty=" + Qty);
                            if (Program._dbg) Console.WriteLine("Prc=" + Price);
                            if (Program._dbg) Console.WriteLine("Time=" + Time);
                            if (Program._dbg) Console.WriteLine("Side=" + Side);
                            if (Program._dbg) Console.WriteLine("OrderId=" + OrderId);
                            if (Program._dbg) Console.WriteLine("SelectTradeId=" + SelectTradeId);
                            if (Program._dbg) Console.WriteLine("ClOrderId=" + ClOrderId);
                            if (Program._dbg) Console.WriteLine("Party_ExecutingFirm=" + Party_ExecutingFirm);
                            if (Program._dbg) Console.WriteLine("Party_ClearingFirm=" + Party_ClearingFirm);
                            if (Program._dbg) Console.WriteLine("Party_EnteringFirm=" + Party_EnteringFirm);
                            if (Program._dbg) Console.WriteLine("Party_Trader=" + Party_Trader);

                            String Last = "";
                            if (value.isSetField(912)) Last = value.getField(912);

                            if (Program._dbg) Console.WriteLine("Last=" + Last);

                            addTrade(CFICode, Symbol, Symbol1, CFICode1, PromptType1, MaturityDate1, MaturityRollingPrompt1, Side1, Price1, Qty1, Symbol2, CFICode2, PromptType2, MaturityDate2, MaturityRollingPrompt2, Side2, Price2, Qty2, Qty, Price, Time, Side, OrderId, ClOrderId, Party_ExecutingFirm, Party_ClearingFirm, Party_EnteringFirm, Party_Trader, InfoText, InternalCrossTrade, SelectTradeId);
                            if (Last == "Y") storeTrades();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
                else if (value is QuickFix44.TradeCaptureReportRequestAck)
                {
                    if (Program._dbg) Console.WriteLine("fromApp --> TradeCaptureReportRequestAck");
                    Program.feedComplete = true;
                }
            }

            //
            // Functions to store order and trade data
            public DataTable tradeDataTable = new DataTable();

            public void initialiseDataTables()
            {
                tradeDataTable.Columns.Add("CFICode", typeof(String));
                tradeDataTable.Columns.Add("Symbol", typeof(String));

                tradeDataTable.Columns.Add("Symbol1", typeof(string));
                tradeDataTable.Columns.Add("CFICode1", typeof(string));
                tradeDataTable.Columns.Add("PromptType1", typeof(String));
                tradeDataTable.Columns.Add("MaturityDate1", typeof(String));
                tradeDataTable.Columns.Add("MaturityRollingPrompt1", typeof(String));
                tradeDataTable.Columns.Add("Price1", typeof(String));
                tradeDataTable.Columns.Add("Qty1", typeof(String));
                tradeDataTable.Columns.Add("Side1", typeof(String));

                tradeDataTable.Columns.Add("Symbol2", typeof(string));
                tradeDataTable.Columns.Add("CFICode2", typeof(string));
                tradeDataTable.Columns.Add("PromptType2", typeof(String));
                tradeDataTable.Columns.Add("MaturityDate2", typeof(String));
                tradeDataTable.Columns.Add("MaturityRollingPrompt2", typeof(String));
                tradeDataTable.Columns.Add("Price2", typeof(String));
                tradeDataTable.Columns.Add("Qty2", typeof(String));
                tradeDataTable.Columns.Add("Side2", typeof(String));

                tradeDataTable.Columns.Add("Qty", typeof(int));
                tradeDataTable.Columns.Add("Price", typeof(double));
                tradeDataTable.Columns.Add("Time", typeof(String));
                tradeDataTable.Columns.Add("Side", typeof(String));
                tradeDataTable.Columns.Add("OrderId", typeof(String));
                tradeDataTable.Columns.Add("ClOrderId", typeof(String));
                tradeDataTable.Columns.Add("PartyExecutingFirm", typeof(String));
                tradeDataTable.Columns.Add("PartyClearingFirm", typeof(String));
                tradeDataTable.Columns.Add("PartyEnteringFirm", typeof(String));
                tradeDataTable.Columns.Add("PartyTrader", typeof(String));
                tradeDataTable.Columns.Add("TradeDate", typeof(DateTime));
                tradeDataTable.Columns.Add("InfoText", typeof(String));
                tradeDataTable.Columns.Add("InternalCrossTrade", typeof(String));
                tradeDataTable.Columns.Add("SelectTradeId", typeof(String));
            }

            public void addTrade(String CFICode, String Symbol, String Symbol1, String CFICode1, String PromptType1, String MaturityDate1, String MaturityRollingPrompt1, String Side1, String Price1, String Qty1, String Symbol2, String CFICode2, String PromptType2, String MaturityDate2, String MaturityRollingPrompt2, String Side2, String Price2, String Qty2, String Qty, String Price, String Time, String Side, String OrderId, String ClOrderId, String Party_ExecutingFirm, String Party_ClearingFirm, String Party_EnteringFirm, String Party_Trader, String InfoText, String InternalCrossTrade, String SelectTradeId)
            {
                if (Program._dbg) Console.WriteLine("adding trade .........");
                int qty = Convert.ToInt32(Qty);
                double prc = Convert.ToDouble(Price);

                if (Program._dbg) Console.WriteLine("qty=" + qty + ", price=" + Price);

                DateTime dt = new DateTime();
                try
                {
                    String[] dta = Time.Split('.');
                    String dtStr = dta[0].Replace('-', ' ');
                    Console.WriteLine("Parsing " + dtStr);
                    String format =  "yyyyMMdd HH:mm:ss" ;
                    dt = DateTime.ParseExact(dtStr, format, CultureInfo.InvariantCulture);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Console.WriteLine("" + Time + " --> " + dt);
                tradeDataTable.Rows.Add(CFICode, Symbol, Symbol1, CFICode1, PromptType1, MaturityDate1, MaturityRollingPrompt1, Price1, Qty1, Side1, Symbol2, CFICode2, PromptType2, MaturityDate2, MaturityRollingPrompt2, Price2, Qty2, Side2, qty, prc, Time, Side, OrderId, ClOrderId, Party_ExecutingFirm, Party_ClearingFirm, Party_EnteringFirm, Party_Trader, dt, InfoText, InternalCrossTrade, SelectTradeId);   
                   ;

                if (Program._dbg) Console.WriteLine("Added trade ...");
            }

            public static void clearTrades()
            {
                String server = System.Configuration.ConfigurationManager.AppSettings["dbserver"];
                SqlConnection myConnection = null;
                try
                {
                    myConnection = new SqlConnection("server=" + server + ";database=LmeDropCopy;Trusted_Connection=True");
                    myConnection.Open();
                    SqlCommand myCommand = new SqlCommand("delete from stage.TradeHistory where Time like '" + datestamp + "-%';");
                    myCommand.ExecuteNonQuery();
                    myConnection.Close();
                }
                catch (Exception e)
                {
                }
                finally
                {
                    try
                    {
                        myConnection.Close();
                    }
                    catch (Exception e)
                    {
                    }
                }

            }
            public void storeTrades()
            {
                String server = System.Configuration.ConfigurationManager.AppSettings["dbserver"];

                try
                {
                    SqlBulkCopy sbc = new SqlBulkCopy("server=" + server + ";database=LmeDropCopy;Trusted_Connection=True");
                    sbc.DestinationTableName = "stage.TradeHistory";
                    sbc.WriteToServer(tradeDataTable);
                    sbc.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                if (Program._dbg) Console.WriteLine("Stored Trades ......");

                //
                // Mark the feed as complete
                Program.feedComplete = true;
            }
        }
    }


