// LmeCppWrapper.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

/*
 * This code uses the library "CryptoPP v5.6.1" available in the FIX API Kit or for download from
 *
 *    http://prdownloads.sourceforge.net/cryptopp/cryptopp561.zip
 *
 * and relies on GCC.
 */
#include <cryptlib.h>
#include <hmac.h>
#include <sha.h>
#include <stdio.h>
#include <iostream>
#include <windows.h>
#include <time.h>

// prints the array supplied in pArray as a string to cout.
char *byteArrayToString (byte* pArray, int pSize)
{
	const char* tHexMap = "0123456789abcdef";
    int tVal;
    char* tRetBuf = (char*) calloc(sizeof(char), pSize * 2 + 1);
    int i = 0;

	for (i = 0; i < pSize; i++)
	{
        tVal = pArray[i];
        if (tVal < 0)
            tVal = 256 + tVal;

        tRetBuf[2*i + 0] = tHexMap[tVal / 16];
        tRetBuf[2*i + 1] = tHexMap[tVal % 16];
    }

    tRetBuf[2*i + 2] = 0;
    return tRetBuf;
}

int valueOf(char pChar)
{
    int tChar = 0;

    tChar = (pChar >='A' && pChar <='F') ? pChar + 'a' - 'A' : pChar;
    if (tChar >= '0' && tChar <= '9')
        return tChar - '0';

    if (tChar >= 'a' && tChar <= 'f')
        return tChar - 'a' + 10;

    printf("not a hexadecimal digit: %c (0x%x)\n", tChar, tChar);
    exit(3);
}

byte* stringToByteArray(char* pString, int pOptTestLen)
{
    int tLen = strlen(pString);

    if ((tLen / 2) * 2 != tLen)
    {
        printf("Funny hex string with length %d\n", tLen);
        exit(3);
    }

    if (pOptTestLen > 0 && tLen != pOptTestLen)
    {
        printf("Length of this field should be %d, not %d\n",
               pOptTestLen, tLen);
        exit(3);
    }

    int tSize = tLen / 2;
    byte* tRetBuf = (byte*) calloc(sizeof(byte), tSize);

    for (int p = 0; p < tSize; p++)
    {
        int tHiNibble = valueOf(pString[p * 2]);
        int tLoNibble = valueOf(pString[p * 2 + 1]);

        int tValue = tHiNibble * 16 + tLoNibble;

        tRetBuf[p] = (char) (tValue > 127 ? tValue - 256 : tValue);
    }

    return tRetBuf;
}


// This function emulates the one used in Java (com.cinnober.common.hermes.security.impl.diffiehellman.Hash)
void hashPassword(byte* pResult,
                  char* pPassword,
                  int pPasswordLength)
{
	const byte salt[] =
    {
		(byte) 0x5C, (byte) 0x8D, (byte) 0x0E, (byte) 0x8A,
		(byte) 0x38, (byte) 0x8A, (byte) 0x10, (byte) 0x76
	};

    byte* pPasswordBytes = (byte*) calloc(sizeof(byte), pPasswordLength);

    for (int i = 0; i < pPasswordLength; i++)
    {
        pPasswordBytes[i] = (byte) pPassword[i];
    }


	CryptoPP::SHA hashTransformation;

	hashTransformation.Update(salt, sizeof(salt));
    hashTransformation.Update(pPasswordBytes, pPasswordLength);
	hashTransformation.Final(pResult);
}

byte* longToBytes(long long int pValue)
{
    byte* tResult;

    if (pValue <= 0)
    {
        return NULL;
    }

    tResult = (byte*) malloc(8);

    tResult[0] = (byte) ((pValue & 0x00000000ff000000L) >> 24);
    tResult[1] = (byte) ((pValue & 0x0000000000ff0000L) >> 16);
    tResult[2] = (byte) ((pValue & 0x000000000000ff00L) >> 8);
    tResult[3] = (byte)  (pValue & 0x00000000000000ffL);

    tResult[4] = tResult[0];
    tResult[5] = tResult[1];
    tResult[6] = tResult[2];
    tResult[7] = tResult[3];

    return tResult;
}

extern "C" __declspec(dllexport) int lmePassword(uint64_t millis, char *tFaxKeyString, char *tPassword, char *output)
{
	printf("got %ld\n",millis);
	
		//long int tTimeInMillis = 12345;

	//printf("input=%lld\n",millis);
		//time_t seconds = time(NULL);
		long long int tTimeInMillis = millis; //(long)seconds*1000;

		printf("Inputs: %lld,%s,%s\n",tTimeInMillis,tFaxKeyString,tPassword);

		Sleep(1000);

	// MAC Example

    //long long int tTimeInMillis;
    //char* tFaxKeyString = (char*) calloc(sizeof(byte), 128);
    //char* tPassword = (char*) calloc(sizeof(char), 128);

    //printf("Current system time in millis: ");
    //scanf("%lli", &tTimeInMillis);
    //printf ("Got %lli\n", tTimeInMillis);

   // printf("Fax key: ");
    //scanf("%64s", tFaxKeyString);

    byte* tFaxKey = stringToByteArray(tFaxKeyString, 64);
    printf("Got:     %s\n", byteArrayToString(tFaxKey, 32));
	Sleep(1000);
    //printf("Cleartext password: ");
    //scanf("%s", tPassword);
    printf("Got:                %s\n", tPassword);

    printf("hashing the fax key...");
	// Get the MAC object.
	CryptoPP::HMAC<CryptoPP::SHA> tHMac(tFaxKey, 32);

    printf("[OK]\nlongToBytes...");
    tHMac.Update(longToBytes(tTimeInMillis), 8);

	printf("[OK]\nDone Mac.Update ...");
	// Get the size of the resulting MAC value.
	int tSize = tHMac.DigestSize();

	// Allocated memory for the result.
	byte* tHashedPassword = (byte*) calloc(sizeof(byte), tSize);

    printf("[OK]\nhashing...");
    hashPassword(tHashedPassword, tPassword, strlen(tPassword));
    printf("[OK]\nupdating...");
	tHMac.Update(tHashedPassword, tSize); // 8?
    printf("[OK]\nfinalizing...");

    byte* tEncryptedPassword = (byte*) calloc(sizeof(byte), tSize);

    tHMac.Final(tEncryptedPassword);
    printf("[OK]\n");

    //return byteArrayToString(tEncryptedPassword, tSize);

	char *ePassword = byteArrayToString(tEncryptedPassword, tSize);
	int len = strlen(ePassword);

	printf("pwd=>>%s<<\n",ePassword);
	printf("len=%d  tsz=%d\n",len,tSize);

	printf("calloc for %d chars\n",len+1);

	int sz = (tSize * 2) + 1;

	printf("sz=%d\n",sizeof(ePassword));
	//output = (char*) calloc(sizeof(char), sz);

	printf("out len=%d\n",strlen(output));

	strcpy_s(output, sz, ePassword);
	printf("out len=%d\n",strlen(output));
	printf("C++ %s --> %s\n",tPassword,output);
	//return output;
	return 0;
}

//
// Uncomment this function to test the LmeCppWrapper as a Windows Console Application
//void main(int argc, const char** argv) {
//	printf("Testing ...");
//	Sleep(1000);
//	long millis = 123456;
//	char *key = "0108D6025DF13E6535BC57554B2299D25268F7B6492520B17EC29E762D78BCFF";
//	char *pwd = "ADMR7DCFIX";
//	printf("Using key=%s and pwd=%s\n",key,pwd);
//
//	Sleep(1000);
//	printf("password=%s\n",lmePassword(millis,key,pwd));
//
//	Sleep(5000);
//}

