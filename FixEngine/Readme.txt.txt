This directory contains working files that are maintained by the QuickFix engine.  If you need to reset the state of the engine, delete the files in the relevant sub-directory.

You need to do this from time to time when going through certiFIX to reset the state of everything, and also click the 'Reset Sequence Numbers' on the web application.