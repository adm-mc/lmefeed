﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using QuickFix;
using System.Threading;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace LmeOrderFeed
{
    
    public partial class LmeOrderService : ServiceBase
    {
        public static Boolean _dbg = false;

        


        static StringBuilder lmeKey = new StringBuilder(System.Configuration.ConfigurationManager.AppSettings["lmekey"]); //new StringBuilder("0108D6025DF13E6535BC57554B2299D25268F7B6492520B17EC29E762D78BCFF");
        static StringBuilder lmePwd = new StringBuilder(System.Configuration.ConfigurationManager.AppSettings["lmepwd"]); //new StringBuilder("ADMR7DCFIX");
        public static String userID = System.Configuration.ConfigurationManager.AppSettings["lmeusr"]; //"ADMR7DCFIX";
        public static StringBuilder sb = new StringBuilder(1000);  // Make sure this is at least 1000 chars long.
        public static String pwdID = null;
        public static long millis = 0;
        public static Session mySession = null;
        public static Boolean feedComplete = false;
        public static SocketInitiator initiator = null;
        public static EventLog myEventLog = null;
        

        public static void recalcPassword()
        {
            //
            // Get the system time in GMT
            TimeSpan ts = (TimeSpan)(DateTime.UtcNow - new DateTime(1970, 1, 1));
            millis = (long)ts.TotalMilliseconds;
            if (_dbg) Console.WriteLine("Millis=" + millis);

            //
            // Build an encrypted password based on the LME Crypto engine, Key and current time
            if (_dbg) Console.WriteLine("Passing in " + lmeKey.ToString() + "," + lmePwd.ToString() + "," + millis);
            LmeCppWrapper.lmePassword(millis, lmeKey.ToString(), lmePwd.ToString(), sb);
            if (_dbg) Console.WriteLine("Password (" + lmePwd + ") encrypted to " + sb.ToString());
            pwdID = sb.ToString();
        }

        public static void MainXX(String[] args)
        {
            LmeOrderService service = new LmeOrderService();
            
            if (Environment.UserInteractive)
            {
                service.OnStart(args);
                Console.ReadLine();
                service.OnStop();
            }
            else
            {
                ServiceBase.Run(service);
            }
        }

        public LmeOrderService()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["dbg"] == "1")
            {
                _dbg = true;
            }
            InitializeComponent();
            if (!System.Diagnostics.EventLog.SourceExists("LmeOrderSource"))
            {
                System.Diagnostics.EventLog.CreateEventSource("LmeOrderSource", "LmeOrderLog");
            }
            eventLog1.Source = "LmeOrderSource";
            eventLog1.Log = "LmeOrderLog";
            myEventLog = eventLog1;
        }

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("LmeOrderService.OnStart()");
            //
            // Initialise the QuickFix FIX Engine
            ClientInitiator app = new ClientInitiator();
            String initiator_cfg = System.Configuration.ConfigurationManager.AppSettings["initiator.cfg"];
            SessionSettings settings = new SessionSettings(@initiator_cfg); 
            QuickFix.Application application = new ClientInitiator();
            FileStoreFactory storeFactory = new FileStoreFactory(settings);
            ScreenLogFactory logFactory = new ScreenLogFactory(settings);
            MessageFactory messageFactory = new DefaultMessageFactory();

            initiator = new SocketInitiator(application, storeFactory, settings, logFactory, messageFactory);
            initiator.start();

            //
            // Pause to let initial handshaking all go through
            Thread.Sleep(3000);    
            System.Collections.ArrayList list = initiator.getSessions();
            SessionID sessionID = (SessionID)list[0];
            Thread.Sleep(5000);

            eventLog1.WriteEntry("LmeOrderService.OnStart() Complete.");

            //ClientInitiator.sendTestMessage(sessionID);

            Thread.Sleep(5000);
            //initiator.stop();  // Had to do this to pass Certifix S11 test where it needed a logout to be sent
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("LmeOrderService.OnStop()");
            //
            // All Done
            initiator.stop();
        }
    }
    class LmeCppWrapper
    {
        // DO NOT CHANG THIS - This needs to tie precisely into the C++ side of things
        [DllImport(@"C:\LmeFeed\LmeOrderFeed\Debug\LmeCppWrapper.dll", EntryPoint = "lmePassword", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
        public static extern int lmePassword([In] long millis, [In] String tFaxKeyString, [In] String tPassword, [Out] StringBuilder ePassword);
    }
    public class ClientInitiator : QuickFix.Application
    {
        public SessionID mySessionID = null;
        int nLogins = 0;

        //
        // Am keeping track of the latest esquence numbers as on a failover LME require tag 789 to be set to this value
        // Newer versions of QuickFix in Java have a configuration parameter set for this, but not in the C# world ....
        public int LatestSeqNum = 0;  
        public void updateLatestSeqNum(Message msg)
        {
            try
            {
                if (msg.isSetField(369)) LatestSeqNum = Convert.ToInt32(msg.getField(369));
            }
            catch (Exception e)
            {
            }
        }







        public static void sendTestMessage(SessionID sessionID)
        {
            //
            // Send a test message - needed for Certification
            try
            {
                QuickFix44.TestRequest tr = new QuickFix44.TestRequest();
                tr.setField(112, "1");
                Session.sendToTarget(tr, sessionID);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public void onCreate(SessionID value)
        {
            //LmeOrderService.myEventLog.WriteEntry("Message OnCreate --> " + value.toString());
            initialiseDataTables();
        }

        public void onLogon(QuickFix.SessionID value)
        {
            logMessage("Called onLogon --> " + value.toString());
            //sendTestMessage();
        }

        public void onLogout(QuickFix.SessionID value)
        {
            LmeOrderService.myEventLog.WriteEntry("Called onLogout --> " + value.toString());
            logMessage("Called onLogout --> " + value.toString());
        }
        public void toAdmin(QuickFix.Message value, QuickFix.SessionID session)
        {
            updateLatestSeqNum(value);
            if (LmeOrderService._dbg)
            {
                logMessage("----------------------------------------------------------------");
                logMessage("Called toAdmin -->toAdmin " + value.ToString());
                logMessage("----------------------------------------------------------------");
            }
            
            if (isMessageOfType(value, MsgType.LOGON))
            {
                LmeOrderService.myEventLog.WriteEntry("*** LOGON MESSAGE ***");
                addLogonField(value);
                // Enable this to force a sequence reset (apparently).  Doesnt seem to work reliably though with the LME
                //if (nLogins % 2 == 0)  value.setBoolean(QuickFix.ResetSeqNumFlag.FIELD, true);
            }
        }
        private void addLogonField(QuickFix.Message message)
        {
            //
            // Recalculate the password.  As this is part encrypted with the current milliseconds, it needs to be done every time
            LmeOrderService.recalcPassword();
            message.getHeader().setField(789, "" + (LatestSeqNum+1));

            //
            // Add key logon information
            message.getHeader().setField(553, LmeOrderService.userID);
            message.getHeader().setField(554, LmeOrderService.pwdID);
            String milliStr = "" + LmeOrderService.millis;
            message.getHeader().setField(95, "" + milliStr.Length);
            message.getHeader().setField(96, "m:" + milliStr);
            //if (nLogins == 0) 
            //message.getHeader().setField(141, "Y"); // Reset all Sequence Numbers
            nLogins++;
            if (LmeOrderService._dbg)
            {
                // Show key feilds that have been added to the logon message
                int[] tags = { 553, 554, 49, 56, 95, 96 };
                for (int i = 0; i < tags.Length; i++)
                {
                    try
                    {
                        Console.WriteLine(" ** TAG[" + tags[i] + "] --> " + message.getHeader().getField(tags[i]));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }

            if (LmeOrderService._dbg)
            {
                // More debugging
                logMessage("----- Logon message below");
                logMessage(message.ToString());
            }
            
        }

        private Boolean isMessageOfType(QuickFix.Message message, String type)
        {
            try
            {
                return type.Equals(message.getHeader().getField(new MsgType()).getValue());
            }
            catch (FieldNotFound e)
            {
                return false;
            }
        }
        public void toApp(QuickFix.Message value, QuickFix.SessionID session)
        {
            updateLatestSeqNum(value);
            if (LmeOrderService._dbg) LmeOrderService.myEventLog.WriteEntry("Called toApp --> " + value.ToString());
        }

        public void fromAdmin(QuickFix.Message value, SessionID session)
        {
            updateLatestSeqNum(value);
            if (LmeOrderService._dbg)
            {
                logMessage("----------------------------------------------------------------");
                logMessage("Called fromAdmin -->fromAdmin " + value.ToString());
                logMessage("----------------------------------------------------------------");
            }
        }
        public void fromApp(QuickFix.Message value, SessionID session)
        {
            updateLatestSeqNum(value);

            if (LmeOrderService._dbg)
            {
                logMessage("----------------------------------------------------------------");
                logMessage("Called fromApp -->fromApp " + value.ToString());
                logMessage("----------------------------------------------------------------");
            }
            if (value is QuickFix44.News)
            {
                QuickFix44.News news = (QuickFix44.News)value;
                int nlines = news.getLinesOfText().getValue();
                String headline = news.getHeadline().ToString();
                StringBuilder msg = new StringBuilder();
                String SendingTime = "";
                QuickFix44.Message.Header mh = value.getHeader();

                if (mh.isSetField(52)) SendingTime = mh.getField(52);
                logMessage("NEWS::" + headline + " --> " + nlines + " lines of text");

                QuickFix44.News.LinesOfText g1 = new QuickFix44.News.LinesOfText();
                for (uint l = 1; l <= nlines; l++)
                {
                    QuickFix44.News.LinesOfText g2 = (QuickFix44.News.LinesOfText)news.getGroup(l, g1);
                    if (g2.isSetField(58))
                    {
                        String lineTxt = g2.getField(58);
                        msg.Append(lineTxt + " ");
                    }
                }
                storeNewsItem(SendingTime, headline, msg.ToString());
                if (LmeOrderService._dbg)
                {
                    logMessage("News (" + headline + ") --> " + msg.ToString());
                }
            }
            if (value is QuickFix44.ExecutionReport)
            {
                if (LmeOrderService._dbg) logMessage("GOT EXECUTION REPORT ***********");
                QuickFix44.ExecutionReport er = (QuickFix44.ExecutionReport)value;

                String SendingTime = "";
                String OrderId = "";
                String ClientOrderId = "";
                String OriginalClientOrderId = "";
                String SelectTradeId = "";
                String OrderStatus = "";
                String OrderRejectReason = "";
                String Symbol = "";
                String Maturity = "";
                String PromptType1 = "";
                String MaturityDate1 = "";
                String MaturityRollingPrompt1 = "";
                String MaturityAveragePrompt1 = "";
                String PromptType2 = "";
                String MaturityDate2 = "";
                String MaturityRollingPrompt2 = "";
                String MaturityAveragePrompt2 = "";

                String CFICode = "";
                String ExecId = "";
                String ExecRefId = "";
                String ExecInst = "";
                String OrderQty = "";
                String FilledQty = "";
                String LeavesQty = "";
                String Price = "";
                String Side = "";
                String TimeInForce = "";
                String CumQty = "";
                String AvgPrice = "";
                String Party_ExecutingFirm = "";
                String Party_ClearingFirm = "";
                String Party_EnteringFirm = "";
                String Party_Trader = "";
                String InfoText = "";

                QuickFix44.Message.Header mh = value.getHeader();

                if (mh.isSetField(52)) SendingTime = mh.getField(52);
                if (er.isSetField(10022)) SelectTradeId = er.getField(10022);
                if (er.isSetField(37)) OrderId = er.getField(37);
                if (er.isSetField(11)) ClientOrderId = er.getField(11);
                if (er.isSetField(41)) OriginalClientOrderId = er.getField(41);
                if (er.isSetField(39))
                {
                    OrderStatus = er.getField(39);
                    if (OrderStatus.Equals("0")) OrderStatus = "New";
                    if (OrderStatus.Equals("1")) OrderStatus = "Partially Filled";
                    if (OrderStatus.Equals("2")) OrderStatus = "Filled";
                    if (OrderStatus.Equals("3")) OrderStatus = "Done for Day";
                    if (OrderStatus.Equals("4")) OrderStatus = "Cancelled";
                    if (OrderStatus.Equals("5")) OrderStatus = "Replaced";
                    if (OrderStatus.Equals("6")) OrderStatus = "Pending Cancel";
                    if (OrderStatus.Equals("8")) OrderStatus = "Rejected";
                    if (OrderStatus.Equals("C")) OrderStatus = "Expired";
                    if (OrderStatus.Equals("E")) OrderStatus = "Pending Replace";
                }
                if (er.isSetField(103))
                {
                    OrderRejectReason = er.getField(103);
                    if (OrderRejectReason.Equals("0")) OrderRejectReason = "Other";
                    if (OrderRejectReason.Equals("1")) OrderRejectReason = "Unknown Symbol";
                    if (OrderRejectReason.Equals("2")) OrderRejectReason = "Exchange Closed";
                    if (OrderRejectReason.Equals("3")) OrderRejectReason = "Order Exceeds Limit";
                    if (OrderRejectReason.Equals("5")) OrderRejectReason = "Unknown Order";
                    if (OrderRejectReason.Equals("6")) OrderRejectReason = "Duplicate Order";
                    if (OrderRejectReason.Equals("13")) OrderRejectReason = "Incorrect Quantity";
                    if (OrderRejectReason.Equals("15")) OrderRejectReason = "Unknown Accounts";
                }
                if (er.isSetField(54))
                {
                    Side = er.getField(54);
                    if (Side.Equals("1")) Side = "B";
                    if (Side.Equals("2")) Side = "S";
                }
                if (er.isSetField(38)) OrderQty = er.getField(38);
                if (er.isSetField(14)) FilledQty = er.getField(14);
                if (er.isSetField(44)) Price = er.getField(44);
                if (er.isSetField(59))
                {
                    TimeInForce = er.getField(59);
                    if (TimeInForce.Equals("0")) TimeInForce = "Day";
                    if (TimeInForce.Equals("1")) TimeInForce = "Good til Cancel";
                    if (TimeInForce.Equals("3")) TimeInForce = "Immediate or Cancel";
                    if (TimeInForce.Equals("8")) TimeInForce = "Good til Ring";

                }
                if (er.isSetField(151)) LeavesQty = er.getField(151);
                if (er.isSetField(14)) CumQty = er.getField(14);
                if (er.isSetField(6)) AvgPrice = er.getField(6);
                if (er.isSetField(17)) ExecId = er.getField(17);
                if (er.isSetField(19)) ExecRefId = er.getField(19);
                if (er.isSetField(18))
                {
                    ExecInst = er.getField(18);
                    if (ExecInst.Equals("S")) ExecInst = "Suspended";
                    if (ExecInst.Equals("2")) ExecInst = "Active";
                }
                if (value.isSetField(461)) CFICode = value.getField(461);
                if (value.isSetField(55)) Symbol = value.getField(55);
                if (value.isSetField(10000)) Maturity = value.getField(10000);
                if (value.isSetField(58)) InfoText = value.getField(58);

                //
                // NoOfInstrumentLegs
                int nLegs = 1;
                try
                {
                    if (value.isSetField(10010)) nLegs = Convert.ToInt32(value.getField(10010));
                }
                catch (Exception e)
                {
                }
                if (LmeOrderService._dbg) logMessage("NumLegs=" + nLegs);
                QuickFix.Group g = new QuickFix.Group(10010, 1);
                er.getGroup(1, g);
       
                if (g.isSetField(541)) MaturityDate1 = g.getField(541);
                if (g.isSetField(10004)) PromptType1 = g.getField(10004);
                if (g.isSetField(10000)) MaturityRollingPrompt1 = g.getField(10000);
                if (g.isSetField(10001)) MaturityAveragePrompt1 = g.getField(10001);
                if (LmeOrderService._dbg) logMessage("Side1 = " + MaturityDate1 + "/" + PromptType1 + "/" + MaturityRollingPrompt1 + "/" + MaturityAveragePrompt1);

                if (nLegs == 2)
                {
                    g = er.getGroup(2, g);
                    if (g.isSetField(541)) MaturityDate2 = g.getField(541);
                    if (g.isSetField(10004)) PromptType2 = g.getField(10004);
                    if (g.isSetField(10000)) MaturityRollingPrompt2 = g.getField(10000);
                    if (g.isSetField(10001)) MaturityAveragePrompt2 = g.getField(10001);
                    logMessage("Side2 = " + MaturityDate2 + "/" + PromptType2 + "/" + MaturityRollingPrompt2 + "/" + MaturityAveragePrompt2);

                }
                
                //
                // Parties
                QuickFix44.ExecutionReport.NoPartyIDs group1 = new QuickFix44.ExecutionReport.NoPartyIDs();
                for (uint p = 1; p < 5; p++)
                {
                    try
                    {
                        QuickFix44.ExecutionReport.NoPartyIDs group2 = (QuickFix44.ExecutionReport.NoPartyIDs) er.getGroup(p, group1);
                        String role = group2.getField(452);
                        String val = group2.getField(448);
                        if (role == "1") Party_ExecutingFirm = val;
                        if (role == "4") Party_ClearingFirm = val;
                        if (role == "7") Party_EnteringFirm = val;
                        if (role == "36") Party_Trader = val;
                    }
                    catch (Exception e)
                    {
                    }
                }

                addOrder(SendingTime, OrderId, ClientOrderId, OriginalClientOrderId, SelectTradeId, OrderStatus, OrderRejectReason, Symbol, PromptType1, MaturityDate1, MaturityRollingPrompt1, MaturityAveragePrompt1, PromptType2, MaturityDate2, MaturityRollingPrompt2, MaturityAveragePrompt2, CFICode, ExecId, ExecRefId, ExecInst, OrderQty, FilledQty, LeavesQty, CumQty, Price, AvgPrice, Side, TimeInForce, Party_ExecutingFirm, Party_ClearingFirm, Party_EnteringFirm, Party_Trader, InfoText);
            }
        }

        //
        // Functions to store order and trade data.  Uses a DataTable to store thedata and SQlBulkInsert to pop it in the database
        public DataTable orderDataTable = new DataTable();

        public void initialiseDataTables()
        {
            orderDataTable.Columns.Add("SendingTime", typeof(String));
            orderDataTable.Columns.Add("SendingDateTime", typeof(DateTime));
            orderDataTable.Columns.Add("OrderId", typeof(String));
            orderDataTable.Columns.Add("ClientOrderId", typeof(String));
            orderDataTable.Columns.Add("OriginalClientOrderId", typeof(String));
            orderDataTable.Columns.Add("SelectTradeId", typeof(String));
            orderDataTable.Columns.Add("OrderStatus", typeof(String));
            orderDataTable.Columns.Add("OrderRejectReason", typeof(String));
            orderDataTable.Columns.Add("Symbol", typeof(String));
            orderDataTable.Columns.Add("PromptType1", typeof(String));
            orderDataTable.Columns.Add("MaturityDate1", typeof(String));
            orderDataTable.Columns.Add("MaturityRollingPrompt1", typeof(String));
            orderDataTable.Columns.Add("MaturityAveragePrompt1", typeof(String));
            orderDataTable.Columns.Add("PromptType2", typeof(String));
            orderDataTable.Columns.Add("MaturityDate2", typeof(String));
            orderDataTable.Columns.Add("MaturityRollingPrompt2", typeof(String));
            orderDataTable.Columns.Add("MaturityAveragePrompt2", typeof(String));
            orderDataTable.Columns.Add("CFICode", typeof(String));
            orderDataTable.Columns.Add("ExecId", typeof(String));
            orderDataTable.Columns.Add("ExecRefId", typeof(String));
            orderDataTable.Columns.Add("ExecInst", typeof(String));
            orderDataTable.Columns.Add("OrderQty", typeof(int));
            orderDataTable.Columns.Add("LeavesQty", typeof(int));
            orderDataTable.Columns.Add("FilledQty", typeof(int));
            orderDataTable.Columns.Add("CumQty", typeof(int));
            orderDataTable.Columns.Add("Price", typeof(double));
            orderDataTable.Columns.Add("AvgPrice", typeof(double));
            orderDataTable.Columns.Add("Side", typeof(String));
            orderDataTable.Columns.Add("TimeInForce", typeof(String));
            orderDataTable.Columns.Add("PartyExecutingFirm", typeof(String));
            orderDataTable.Columns.Add("PartyClearingFirm", typeof(String));
            orderDataTable.Columns.Add("PartyEnteringFirm", typeof(String));
            orderDataTable.Columns.Add("PartyTrader", typeof(String));
            orderDataTable.Columns.Add("InfoText", typeof(String));
            LmeOrderService.myEventLog.WriteEntry("Initialise Data Tables");
        }
        public void addOrder(String SendingTime, String OrderId, String ClientOrderId, String OriginalClientOrderId, String SelectTradeId, String OrderStatus, String OrderRejectReason, String Symbol, String PromptType1, String MaturityDate1, String MaturityRollingPrompt1, String MaturityAveragePrompt1, String PromptType2, String MaturityDate2, String MaturityRollingPrompt2, String MaturityAveragePrompt2, String CFICode,
            String ExecId, String ExecRefId, String ExecInst, String OrderQty, String FilledQty, String LeavesQty, String CumQty, String Price, String AvgPrice, String Side, String TimeInForce, String Party_ExecutingFirm, String Party_ClearingFirm, String Party_EnteringFirm, String Party_Trader, String InfoText)
        {
            if (LmeOrderService._dbg) logMessage("adding order ........." + OrderId);
            int iOrderQty = 0;
            try {
                iOrderQty = Convert.ToInt32(OrderQty);
            } catch(Exception e) {
            }

            int iLeavesQty = 0;
            try
            {
                iLeavesQty = Convert.ToInt32(LeavesQty);
            }
            catch (Exception e)
            {
            }

            int iFilledQty = 0;
            try
            {
                iFilledQty = Convert.ToInt32(FilledQty);
            }
            catch (Exception e)
            {
            }

            int iCumQty = 0;
            try {
                iCumQty = Convert.ToInt32(CumQty);
            }
            catch (Exception e)
            {
            }

            double dPrice = 0;
            try {
                dPrice = Convert.ToDouble(Price);
            }
            catch (Exception e)
            {
            }

            double dAvgPrice = 0;
            try
            {
                dAvgPrice = Convert.ToDouble(AvgPrice);
            }
            catch (Exception e)
            {
            }

            DateTime SendingDateTime = new DateTime();
            try
            {
                if (LmeOrderService._dbg) logMessage("Parsing SendingTime=" + SendingTime);
                String[] dta = SendingTime.Split('.');
                String dtStr = dta[0].Replace('-', ' ');
                if (LmeOrderService._dbg) logMessage("dtStr=" + dtStr);
                //Console.WriteLine("Parsing " + dtStr);
                String format = "yyyyMMdd HH:mm:ss";
                SendingDateTime = DateTime.ParseExact(dtStr, format, CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                logMessage(e.Message);
                LmeOrderService.myEventLog.WriteEntry("addOrder() Error-->" + e.Message);
            }

            if (LmeOrderService._dbg) logMessage("" + SendingTime + " --> " + SendingDateTime);
            orderDataTable.Rows.Add(SendingTime, SendingDateTime, OrderId, ClientOrderId, OriginalClientOrderId, SelectTradeId, OrderStatus, OrderRejectReason, Symbol, PromptType1, MaturityDate1, MaturityRollingPrompt1, MaturityAveragePrompt1, PromptType2, MaturityDate2, MaturityRollingPrompt2, MaturityAveragePrompt2, CFICode, ExecId, ExecRefId, ExecInst, iOrderQty, iFilledQty, iLeavesQty, iCumQty, dPrice, dAvgPrice, Side, TimeInForce, Party_ExecutingFirm, Party_ClearingFirm, Party_EnteringFirm, Party_Trader, InfoText);
            storeOrders(true);
            if (LmeOrderService._dbg) logMessage("Added order ...");
        }
        public void storeOrders(Boolean lazy)
        {
            // If performance becomes an issue then consider adding orders in batches.  Ive left the code this way
            // so you can just uncomment the line below to post to SQL after every 10 orders ... As we are storing
            // into Stage tables this probably wont be needed.
            //if (lazy && orderDataTable.Rows.Count < 10) return;

            //
            // Save
            String server = System.Configuration.ConfigurationManager.AppSettings["dbserver"];
            try
            {
                SqlBulkCopy sbc = new SqlBulkCopy("server=" + server + ";database=LmeDropCopy;Trusted_Connection=True");
                sbc.DestinationTableName = "stage.OrderHistory";
                sbc.WriteToServer(orderDataTable);
                sbc.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                LmeOrderService.myEventLog.WriteEntry("storeOrders() Error-->" + e.Message);
            }

            if (LmeOrderService._dbg) logMessage("Stored Trades ......");
            orderDataTable.Clear();
        }

        StreamWriter sw = null;
        public void logMessage(String msg)
        {
            if (sw == null) {
                String dir = System.Configuration.ConfigurationManager.AppSettings["dbglogdir"]; 
                DateTime now = DateTime.Today;

                String nam = dir+"\\log\\OrderFeed-"+now.ToShortDateString().Replace('/','-')+".log";
                if (!File.Exists(nam)) {
                    sw = File.CreateText(nam);
                } else {
                    sw = File.AppendText(nam);
                }

            }
            sw.WriteLine(msg);
            sw.AutoFlush = true;
        }


        public void storeNewsItem(String sendingTime, String headline, String msg)
        {
            String[] dta = sendingTime.Split('.');
            String dtStr = dta[0].Replace('-', ' ');
                
            try
            {
                String server = System.Configuration.ConfigurationManager.AppSettings["dbserver"];
                SqlConnection conn = new SqlConnection("server=" + server + ";database=LmeDropCopy;Trusted_Connection=True");
                conn.Open();
                SqlCommand cmd = new SqlCommand("insert into stage.NewsHistory values('"+dtStr+"','" + sendingTime + "','" + headline + "','" + msg + "');", conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception e)
            {
                // Error
                LmeOrderService.myEventLog.WriteEntry("storeNewsItem() Error-->"+e.Message);
            }
        }
    }
}
