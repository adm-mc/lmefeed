import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

//import com.cinnober.common.hermes.security.impl.diffiehellman.RawBlowfish;

/*
 * Created on Jun 29, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author mjoberg
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EncryptionOfPassword
{
    private static final String cHexDigits = "0123456789abcdef";

    public class Hash 
    {
        private MessageDigest tSha;
    
        public final byte[] shaSalt = 
        {
            (byte) 0x5C, (byte) 0x8D, (byte) 0x0E, (byte) 0x8A,
            (byte) 0x38, (byte) 0x8A, (byte) 0x10, (byte) 0x76

        };
    
        private Hash() 
        {
            try
            {
                tSha = MessageDigest.getInstance("SHA");
            }
            catch (NoSuchAlgorithmException e)
            {
                tSha = null;
            }
        }

        private byte[] getShaHash(byte[] pData, byte[] pSecret)
        {
            byte[] tOutput;
        
            if (tSha == null || pData == null)
            {
                return null;
            }
        
            synchronized (tSha)
            {
            
                tSha.update(shaSalt);
            
                if (pSecret != null)
                {
                    tSha.update(pSecret);
                }
            
                tOutput = tSha.digest(pData);
            
                tSha.reset();
            
                return tOutput;
            }
        }
        public byte[] getShaHashAsBytes (byte[] pData)
        {
            return getShaHash(pData, null);
        }

        public byte[] getShaHashAsBytes(String pData)
        {
            try
            {
                byte[] tData = pData.getBytes("ISO-8859-1");
                return getShaHashAsBytes(tData);
            }
            catch (UnsupportedEncodingException e)
            {
                return null;
            }
        }
    }

    public EncryptionOfPassword()
    {
        String tPasswordInCleartext = readLine("Password in cleartext: ");
        String tFaxKeyInHex = readLine("Fax key: ");
        String tTimeInMillis = readLine("Random number or current time in ms: ");
        byte[] tFaxKey = hexByteDecode(tFaxKeyInHex);
        long tCurrentTimeInMillis = Long.parseLong(tTimeInMillis);
        byte[] tHashedPasswordToBeSentOverTheWire = null;


        // Use JCE and a HMAC to hash a password...        
        try
        {
            Mac mac = Mac.getInstance("HmacSHA1");
            
            SecretKeySpec tSecretUserKey = new SecretKeySpec(tFaxKey, "Blowfish"); // "Blowfish" is only there to fool SecretKeySpec
            mac.init(tSecretUserKey);
            mac.update(convertLong2Iv(tCurrentTimeInMillis));

            /*
             * Pick up the password for encryption and
             * store the encrypted password
             */
            byte[] tHashedPassword = new Hash().getShaHashAsBytes(tPasswordInCleartext.getBytes("ISO-8859-1"));    
            mac.update(tHashedPassword); // Make sure that we're using the correct character encoding.
            tHashedPasswordToBeSentOverTheWire = mac.doFinal();
        }
        catch (Exception e)
        {
            System.err.println("Error while trying to hash the password.");
            e.printStackTrace();
            System.exit(-1);
        }
        
        System.out.println("Encrypted password is: " +
                           hexByteEncode(tHashedPasswordToBeSentOverTheWire));
    }

    public EncryptionOfPassword(String pPasswordInCleartext, String pFaxKeyInHex, String pTimeInMillis)
    {
        byte[] tFaxKey = hexByteDecode(pFaxKeyInHex);
        long tCurrentTimeInMillis = Long.parseLong(pTimeInMillis);
        byte[] tHashedPasswordToBeSentOverTheWire = null;

        // Use JCE and a HMAC to hash a password...        
        try
        {
            Mac mac = Mac.getInstance("HmacSHA1");
            SecretKeySpec tSecretUserKey = new SecretKeySpec(tFaxKey, "Blowfish"); // "Blowfish" is only there to fool SecretKeySpec
            mac.init(tSecretUserKey);
            mac.update(convertLong2Iv(tCurrentTimeInMillis));

            /*
             * Pick up the password for encryption and
             * store the encrypted password
             */
            byte[] tHashedPassword = new Hash().getShaHashAsBytes(pPasswordInCleartext.getBytes("ISO-8859-1"));    
            mac.update(tHashedPassword); // Make sure that we're using the correct character encoding.
            tHashedPasswordToBeSentOverTheWire = mac.doFinal();
        }
        catch (Exception e)
        {
            System.err.println("Error while trying to hash the password.");
            e.printStackTrace();
            System.exit(-1);
        }
        
        System.out.println("Encrypted password is: " + hexByteEncode(tHashedPasswordToBeSentOverTheWire));
    }

    public static byte[] convertLong2Iv (long pValue)
    {
        byte[] tResult;

        if (pValue <= 0L)
        {
            return null;
        }
        
        tResult = new byte[8];

        tResult[0] = (byte) ((pValue & 0x00000000ff000000L) >>> 24);
        tResult[1] = (byte) ((pValue & 0x0000000000ff0000L) >>> 16);
        tResult[2] = (byte) ((pValue & 0x000000000000ff00L) >>> 8);
        tResult[3] = (byte) ( pValue & 0x00000000000000ffL);
        
        tResult[4] = tResult[0];
        tResult[5] = tResult[1];
        tResult[6] = tResult[2];
        tResult[7] = tResult[3];
        return tResult;
    }

    public byte[] hexByteDecode(String pHexEncodedString)
        throws NumberFormatException
    {
        int tStrLen = pHexEncodedString.length();

        if ((tStrLen / 2) * 2 != tStrLen)
            throw new NumberFormatException("odd hex string length: " + tStrLen);

        byte[] tRetBuf = new byte[tStrLen / 2];
        for (int b = 0; b < tRetBuf.length; b++)
        {
            int tHiNibble = valueOf(pHexEncodedString.charAt(b * 2));
            int tLoNibble = valueOf(pHexEncodedString.charAt(b * 2 + 1));
            int tValue = (tHiNibble * 16 + tLoNibble);

            tRetBuf[b] = (byte) (tValue > 127 ? tValue - 256 : tValue);
        }

        return tRetBuf;
    }

    public static String hexByteEncode(byte[] pBuffer)
    {
        StringBuffer tSB = new StringBuffer(pBuffer.length * 2);
        for (int b = 0; b < pBuffer.length; b++)
        {
            int tVal = pBuffer[b];
            if (tVal < 0)
            {
                tVal = 256 + tVal;
            }

            tSB.append(cHexDigits.charAt(tVal / 16));
            tSB.append(cHexDigits.charAt(tVal % 16));
        }

        return new String(tSB);
    }

    public static void main(String[] args)
    {
        new EncryptionOfPassword();

        // Encrypted password should be: 8e391f0db6e7a03d19a596f8a723df6d104708e2
//        new EncryptionOfPassword("fff0", "9732EFDD8496F4D091CDA7D12055E0CB3E0A65E3AEA8FEA553349979A4527A33", "1292937779539");
    }

    private String readLine(String tPromptString)
    {
        byte[] tBuffer = new byte[256];
        int tPos = 0;
        byte tNextByte;

        System.out.print(tPromptString);

        while (true)
        {
            tNextByte = readByte();
            if (tNextByte == '\n')
                break;

            if (tNextByte == '\r')
            {
                //Do nothing.
                continue;
            }

            tBuffer[tPos] = (byte) tNextByte;
            tPos++;
        }

        return new String(tBuffer, 0, tPos);
    }

    public static byte readByte()
    {
        int tByteAsInt = -1;     // initialize the return value in any case
        try
        {
            tByteAsInt = System.in.read();
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
            System.out.println("Fatal error. Ending Program.");
            System.exit(0);
        }

        return (byte) tByteAsInt;
    }

    private int valueOf(char pCh)
        throws NumberFormatException
    {
        int tCh = 0;

        /*
         * lower case if required
         */
        tCh = (pCh >= 'A' && pCh <= 'F') ? pCh + 'a' - 'A' : pCh;
        if (tCh >= '0' && tCh <= '9')
            return (char) (tCh - '0');

        if (tCh >= 'a' && tCh <= 'f')
            return (char) (tCh - 'a' + 10);

        throw new NumberFormatException("not a hex digit: " + pCh);
    }
}
