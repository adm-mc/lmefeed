/*
 * Created on Nov 19, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * @author mjoberg
 *
 * Displays the system time in milliseconds at last midnight and the system time in millis
 * at the following midnight.
 */
public class MillisTest 
{
    public MillisTest()
    {
        SimpleDateFormat tDateFormatHere = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");
        SimpleDateFormat tDateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");
        TimeZone tLocalTimeZone = TimeZone.getDefault();
        TimeZone tGmtTimeZone = TimeZone.getTimeZone("GMT");
        GregorianCalendar tNowInGmt = new GregorianCalendar(tGmtTimeZone);
        GregorianCalendar tLastMidnight = (GregorianCalendar) tNowInGmt.clone();
        GregorianCalendar tNextMidnight = (GregorianCalendar) tNowInGmt.clone();

        tLastMidnight.set(tNowInGmt.get(GregorianCalendar.YEAR),
                          tNowInGmt.get(GregorianCalendar.MONTH),
                          tNowInGmt.get(GregorianCalendar.DAY_OF_MONTH),
                          0, 0, 0);
        tNextMidnight.setTime(tLastMidnight.getTime());
        tNextMidnight.add(GregorianCalendar.DAY_OF_MONTH, 1);
        tDateFormatGmt.setTimeZone(tGmtTimeZone);
        show("          now", tNowInGmt, tDateFormatGmt, tGmtTimeZone);
        show("          now", tNowInGmt, tDateFormatHere, tLocalTimeZone);
        show("last midnight", tLastMidnight, tDateFormatGmt, tGmtTimeZone);
        show("last midnight", tLastMidnight, tDateFormatHere, tLocalTimeZone);
        show("next midnight", tNextMidnight, tDateFormatGmt, tGmtTimeZone);
        show("next midnight", tNextMidnight, tDateFormatHere, tLocalTimeZone);
    }

    public static void main(String[] args)
    {
        new MillisTest();
    }

    public void show(String pText, GregorianCalendar pCalendar,
                      DateFormat pDateFormat, TimeZone pTimeZone)
    {
        System.out.println(pText + ": " + pDateFormat.format(pCalendar.getTime()) +
                           " (" + pTimeZone.getDisplayName() + "), ms=" + pCalendar.getTimeInMillis());
    }
}
