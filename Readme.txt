This folder contains the source and runtime area for the LME feeds.

LmeFeed is an APPLICATION that should be run once daily after the market close to capture TRADE reports
LmeOrderFeed is a SERVICE that needs to run all the time to capture EXECUTION reports (orders etc).  Make sure this is set to auto-start and restart as well.

The FIX44-LME.xml is the XML file that the applications use.  This is based on the standard FIX44.xml file with the custom LME fields added.

FixEngine is the runtime directory used by the applications

The application has to link statically to the WrapperDll (it is unmanaged) and hence it all needs to be run from C:\LmeFeed\.

The 'initiator' files are the configuration files for the FIX engine.

The 'App Config' files are the configuration files for the C# Application


** TradeFeed Files
C:\LmeFeed\live-initiator.cfg
C:\LmeFeed\LmeFeed\LmeFeed\bin\Debug\LmeFeed is the application
C:\LmeFeed\LmeFeed\LmeFeed\bin\Debug\LmeFeed.exe is the app config file
C:\LmeFeed\LmeFeed\LmeFeed.sln is the visual studio solution

Run this at 22:00 from the Task Scheduler

** OrderFeed Files
C:\LmeFeed\live-orderfeed_initiator.cfg
C:\LmeFeed\LmeOrderFeed\LmeOrderFeed\bin\Debug\LmeOrderFeed is the application
C:\LmeFeed\LmeOrderFeed\LmeOrderFeed\bin\Debug\LmeOrderFeed.exe is the app config file
C:\LmeFeed\LmeOrderFeed\LmeOrderFeed.sln is the visual studio solution

Run this as a service with automatic restart
The orderfeed writes to the LmeOrderFeed event log to help with debugging.

