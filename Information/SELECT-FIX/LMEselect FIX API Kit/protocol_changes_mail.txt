FIX Procol Changes at LME
 
As the tests have continued, two weaknesses have been found in the current version of
the FIX server.

It is possible to log on without altering the encrypted password.

The FIX server accepts the logging on with a sequence number greater than one the first
attempt of the day.

We'd like to force the clients to send a new password with each logon attempt, avoiding
hostile third parties
that can eavesdrop another client's network connection (not possible at LME), picking up
another client's password and resusing it. In order to accomplish this, the number in 
he RawData field of the Logon request is required to increase with each Logon attempt. We
suggest that the  RawData field is represented by an 64-bit integer ranging from system
time at midnight the day the client logs on until the system time at midnight the day
after. The trailing Java program demonstrates how to accomplish the current system time 
expressed in millisconds since January 1, 1970 and the same figures for the time at last
and next midnights.

  now: 2003-12-09 07:00:038 (Greenwich Mean Time), ms=1070953238930
  now: 2003-12-09 08:00:038 (Central European Time), ms=1070953238930
  last midnight: 2003-12-09 00:00:000 (Greenwich Mean Time), ms=1070928000930
  last midnight: 2003-12-09 01:00:000 (Central European Time), ms=1070928000930
  next midnight: 2003-12-10 00:00:000 (Greenwich Mean Time), ms=1071014400930
  next midnight: 2003-12-10 01:00:000 (Central European Time), ms=1071014400930

Following this change, the clients need to present an increasing number with the RawData
field and in the range from last midnight to next midnight. We'd recommend not to use the
system's local time but the current time in the GMT time zone. We intend to reject Logon
requests that don't present an encreasing number (since last Logon) in the RawData field.
We will use the code in MillisTest as the foundation for the interval check.

 
