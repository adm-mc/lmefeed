#
# This code uses the library "CryptoPP v5.6.1" available in the FIX API Kit or for download from
#  http://prdownloads.sourceforge.net/cryptopp/cryptopp561.zip
#
# and extracted to the crypto sub-directory. (cryptopp561.zip has been verified to work)
#
cd crypto
make -f GNUmakefile
cd ../
g++ -c -Icrypto -o encryption_of_password.o encryption_of_password.cpp
g++ -o encryption_of_password.exe encryption_of_password.o -lcryptopp -Lcrypto